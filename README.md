# fetus-proxy

NGINX proxy app for our fetus app

## Usage

### Environment Variables 環境變量


 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 *                 監聽端口（默認值：`8000`）
 * `APP_HOST` - Hostname of the app to forward requests to (default: `fetus`)
 *              將請求轉發到的應用程序的主機名（默認：`fetus`）
 * `APP_PORT` - Port of the app to forward requests to (default: `9000`)
 *              將請求轉發到的應用程序端口（默認值：`9000`）



